package cl.grebolledoa.miconversor;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private static final int VALOR_DOLAR = 780;

    private EditText etNumeroIngresado;
    private Button btConvertir;
    private TextView tvError;
    private TextView tvResultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //enlace de las views con nuestros atributos
        this.etNumeroIngresado = findViewById(R.id.et_valor_ingresado);
        this.btConvertir = findViewById(R.id.bt_convertir);
        this.tvError = findViewById(R.id.tv_error);
        this.tvResultado = findViewById(R.id.tv_resultado);
    }

    public void btConvertirOnClick(View v){
        Log.i("btConvertir","Evento click");
        try{
            //obtener el valor del EditText
            String valorEditText = this.etNumeroIngresado.getText().toString();

            int numeroingresado, resultado;

            //convierto mi cadena de caracteres del edittext en una variable de tipo entero
            numeroingresado = Integer.parseInt(valorEditText);

            //realizo la operación
            resultado = numeroingresado * VALOR_DOLAR;

            //obtener nuestro string de resultado desde los recursos
            String textoResultado = getResources().getString(R.string.resultado);

            textoResultado = textoResultado.replace("${valor}", String.valueOf(numeroingresado));
            textoResultado = textoResultado.replace("${resultado}", String.valueOf(resultado));

            //muestro mi resultado en mi TextView
            this.tvResultado.setText(textoResultado);

            this.tvError.setText("");

        } catch(Exception ex){
            //muestro mi error en mi TextView
            this.tvError.setText("Se producido un error ("+ ex.getMessage() + ")");

            //limpiar TextView resultado
            this.tvResultado.setText("");

            Log.e("btConvetir", ex.getMessage());
        }
    }


}